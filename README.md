# ADM Project
### Fall 2015

#### Pratik Agrawal
## Working With The Divvy Dataset
### Introduction
Over the past couple of years Divvy has organize data challenges for invigorating some innovation in the Chicago Data Science community as well as learn new ways to visualize as well as manage the bike rental system. 
### Problem
There are always Divvy van that ferry bikes around from station to station based on lack or surplus of bikes at a given location. This movement of bikes is labor and time intensive. Both of which are high costs that Divvy has to bear. It would be nice to be able to predict the volume of rentals.<br> 
In this project I have decided to work with daily rental volume (total rides) as my target variable, and as this is a __supervised__ learning problem the techniques that would be used are as follows-<br>
a) Lasso Regression<br>
b) Ridge Regression<br>
c) Elastic Net<br>
d) Gradient Boosted Regression<br>
### Data Sets
a) Divvy data set 2015 Q1 & Q2<br>
b) Route Information data- In order to enrich the data set with more information, I decided to include distance information (route calculation from HERE.com Route Calculation API) for each origin/destination pair in the dataset.<br>
c) Weather data- weather data from Wunderground.com was downloaded for the period pertaining to the Divvy data set.